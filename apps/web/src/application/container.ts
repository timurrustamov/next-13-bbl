import { Container } from "inversify";
import AccessTokenProvider from "shared/providers/access-token-provider/access-token-provider.abstract";
import CookieAccessTokenProvider from "shared/providers/platform-specific/access-token-provider/cookie-access-token-provider";
import CharacterRepository from "shared/providers/character-repository/character-repository";
import ApiCharacterRepository from "shared/providers/character-repository/api-character-repository";

const ApplicationContainer = new Container({ defaultScope: "Singleton" });

ApplicationContainer.bind(AccessTokenProvider).toDynamicValue(
  () => new CookieAccessTokenProvider()
);
ApplicationContainer.bind(CharacterRepository).toDynamicValue(
  ({ container }) => new ApiCharacterRepository(container.get(AccessTokenProvider))
);

export default ApplicationContainer;
