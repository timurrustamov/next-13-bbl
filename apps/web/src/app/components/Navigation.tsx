import Categories from "@gojob/style/Icons/Categories";
import Link from "next/link";
import Icon from "./Icon";

export const Navigation = () => {

  return (
    <div className="sticky w-full px-4 py-2 bg-white grid grid-cols-5 place-items-center shadow-positive">
      <Link href="/" className="inline-block" aria-label="Home">
        <Icon className="w-6 h-6 fill-atomOne" />
      </Link>

      <Link
        href="/wiki"
        className="typography-headline-4 col-start-5 flex gap-0.5"
      >
        <Categories className="w-4 h-4 fill-atomOne" />
        Wiki
      </Link>
    </div>
  );
};
