import { motion } from "framer-motion";

interface CharacterBioSkeletonProps {
  layoutId?: string;
}
export const CharacterBioSkeleton = (props: CharacterBioSkeletonProps) => {
  return (
    <div className="grid gap-4 grid-cols-12">
      <motion.h1
        layoutId={props.layoutId ? `${props.layoutId}-title` : undefined}
        className="typography-headline-2 col-span-5 rounded px-2 bg-grey/75 animate-pulse text-transparent"
      >
        Loading...
      </motion.h1>

      <motion.div
        layoutId={props.layoutId ? `${props.layoutId}-image` : undefined}
        className="col-span-10 col-start-2 relative aspect-1 rounded overflow-hidden shadow-positive"
      >
        <div className="absolute inset-0 bg-grey/75 animate-pulse" />
      </motion.div>

      <p className="typography-primary col-span-12 bg-grey/75 px-2 animate-pulse rounded text-transparent min-h-[300px]">
        Loading ...
      </p>
    </div>
  );
};
