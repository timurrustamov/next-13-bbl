import Image from "next/image";
import notFound from "../../assets/404.jpg";

export const CharacterBioNotFound = () => {
  return (
    <div className="grid items-center h-full">
      <div className="grid gap-4">
        <h1 className="typography-headline-1 text-center">404 - Character Not found</h1>
        <Image src={notFound} alt="Not found" />
      </div>
    </div>
  );
};
