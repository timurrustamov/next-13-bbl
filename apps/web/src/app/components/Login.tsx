import PrimaryButton from "@gojob/style/Buttons/Primary";

interface LoginProps {
  onLogin: (token: string) => void;
}
export const Login = (props: LoginProps) => {
  const handleLogin = () => {
    props.onLogin("This is a fake access token");
  };

  return (
    <div className="h-full grid place-items-center">
      <div className="grid gap-4">
        <h1 className="typography-headline-4">Please log in to continue</h1>

        <PrimaryButton className="btn-centered" onClick={handleLogin}>
          Login
        </PrimaryButton>
      </div>
    </div>
  );
};
