"use client";

import { motion } from "framer-motion";
import Image from "next/image";
import { Character } from "shared/models/character/character";

interface CharacterBioProps {
  character: Character;
  layoutId?: string;
}
export const CharacterBio = (props: CharacterBioProps) => {
  return (
    <div className="grid gap-4 grid-cols-12">
      <motion.h1
        layoutId={props.layoutId ? `${props.layoutId}-title` : undefined}
        className="typography-headline-2 col-span-full"
      >
        {props.character.name}
      </motion.h1>

      <motion.div
        layoutId={props.layoutId ? `${props.layoutId}-image` : undefined}
        className="col-span-10 col-start-2 relative aspect-1 rounded overflow-hidden shadow-positive"
      >
        <Image fill alt={props.character.name} src={props.character.image} />
      </motion.div>

      <p className="typography-primary col-span-12">
        Ipsum incididunt sit minim non mollit reprehenderit cupidatat aliqua
        minim ea velit. Exercitation adipisicing commodo elit mollit. Ad
        proident ea nisi aute id occaecat. Nostrud sit adipisicing sit id ipsum.
        Dolor eiusmod sit minim nulla dolore consequat consectetur duis sunt
        reprehenderit id. Fugiat pariatur adipisicing sunt dolore occaecat sint
        fugiat elit proident ullamco. Enim esse consectetur proident quis.
      </p>
    </div>
  );
};
