"use client";

import Image from "next/image";
import GreenChip from "@gojob/style/Chips/Green";
import { Character } from "shared/models/character/character";
import { motion } from "framer-motion";

const MotionImage = motion(Image);

interface CharacterCardProps {
  character: Character;
  layoutId?: string;
}
export const CharacterCard = (props: CharacterCardProps) => {
  return (
    <article className="card grid grid-cols-3">
      <motion.div
        layoutId={props.layoutId ? `${props.layoutId}-image` : undefined}
        className="aspect-1 relative"
      >
        <MotionImage
          fill
          priority
          sizes="(max-width: 768px) 100vw"
          alt={props.character.name}
          src={props.character.image}
        />
      </motion.div>
      <div className="col-span-2 flex p-2 flex-col gap-2 items-start">
        <motion.h2
          layoutId={props.layoutId ? `${props.layoutId}-title` : undefined}
          className="typography-headline-4"
        >
          {props.character.name}
        </motion.h2>

        <GreenChip>{props.character.status}</GreenChip>
      </div>
    </article>
  );
};
