"use client";

import { ReactNode } from "react";

import { Navigation } from "./components/Navigation";

interface RootLayoutProps {
  children: ReactNode;
}

export default function RootLayout(props: RootLayoutProps) {
  return (
    <html lang="en">
      <body className="bg-grey-light h-screen flex flex-col items-stretch">
        <Navigation />

        <main className="p-2 h-full">{props.children}</main>
      </body>
    </html>
  );
}
