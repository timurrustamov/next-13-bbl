import Image from "next/image";
import Link from "next/link";
import cover from "../assets/rick-and-morty.jpg";

export default function RootPage() {
  return (
    <div className="flex flex-col items-center gap-4">
      <h1 className="typography-headline-1 my-4">Rick and Morty API</h1>

      <Link href="/wiki">
        <Image alt="Rick and Morty" src={cover} className="rounded shadow-lg" />
      </Link>
    </div>
  );
}
