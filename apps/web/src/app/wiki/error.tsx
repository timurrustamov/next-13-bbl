"use client";

import AccessTokenProvider from "shared/providers/access-token-provider/access-token-provider.abstract";
import { AccessTokenNotFoundError } from "shared/providers/access-token-provider/errors/access-token-not-found.error";
import ApplicationContainer from "../../application/container";
import { Login } from "../components/Login";

interface WikiErrorPageProps {
  error: Error;
  reset: () => void;
}
export default function WikiErrorPage(props: WikiErrorPageProps) {
  switch (props.error.message) {
    case AccessTokenNotFoundError.message:
      return <AccessTokenNotFoundErrorPage reset={props.reset} />;
    default:
      return <h1>Unhandled error !</h1>;
  }
}

interface AccessTokenNotFoundErrorPageProps {
  reset: () => void;
}
const AccessTokenNotFoundErrorPage = (
  props: AccessTokenNotFoundErrorPageProps
) => {
  const handleLogin = (token: string) => {
    ApplicationContainer.get(AccessTokenProvider).setAccessToken({
      __type: "AccessToken",
      value: token,
    });
    props.reset();
  };

  return <Login onLogin={handleLogin} />;
};
