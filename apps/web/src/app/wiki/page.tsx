import Link from "next/link";
import CharacterRepository from "shared/providers/character-repository/character-repository";
import ApplicationContainer from "../../application/container";
import { CharacterCard } from "../components/CharacterCard";

export default async function WikiPage() {
  const characters = await ApplicationContainer.get(
    CharacterRepository
  ).getAll();

  return (
    <div className="flex flex-col gap-2">
      <h1 className="typography-headline-3">Characters</h1>

      <div className="grid gap-2">
        {characters.map((character) => {
          const href = `/wiki/character/${character.id}` as const;
          return (
            <Link key={character.id} href={href}>
              <CharacterCard character={character} layoutId={href} />
            </Link>
          );
        })}
      </div>
    </div>
  );
}
