import { notFound } from "next/navigation";
import CharacterRepository from "shared/providers/character-repository/character-repository";
import ApplicationContainer from "../../../../application/container";
import { CharacterBio } from "../../../components/CharacterBio";

interface CharacterPageProps {
  params: {
    id: string;
  };
}
export default async function CharacterPage(props: CharacterPageProps) {
  const character = await ApplicationContainer.get(CharacterRepository).getById(
    props.params.id
  );

  if (!character) {
    return notFound();
  }

  return (
    <CharacterBio
      character={character}
      layoutId={"/wiki/character/" + character.id}
    />
  );
}
