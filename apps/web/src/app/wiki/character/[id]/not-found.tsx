import { CharacterBioNotFound } from "../../../components/CharacterBioNotFound";

export default function CharacterNotFound() {
  return <CharacterBioNotFound />;
}
