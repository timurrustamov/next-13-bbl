"use client";

import { usePathname } from "next/navigation";
import { CharacterBioSkeleton } from "../../../components/CharacterBioSkeleton";

export default function CharacterLoading() {
  const pathname = usePathname();

  return <CharacterBioSkeleton layoutId={pathname} />;
}
