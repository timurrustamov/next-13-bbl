'use client';

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactNode } from "react";
import "./global.css";

const client = new QueryClient();

interface RootTemplateProps {
  children: ReactNode;
}
export default function RootTemplate(props: RootTemplateProps) {
  return (
    <QueryClientProvider client={client}>{props.children}</QueryClientProvider>
  );
}
