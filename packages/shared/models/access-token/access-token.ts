export interface AccessToken {
  readonly __type: "AccessToken";
  readonly value: string;
}
