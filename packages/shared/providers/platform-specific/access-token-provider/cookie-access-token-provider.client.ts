import { parseCookies, setCookie } from "nookies";
import { AccessToken } from "../../../models/access-token/access-token";
import AccessTokenProvider, {
  COOKIE_NAME,
} from "../../access-token-provider/access-token-provider.abstract";
import { AccessTokenNotFoundError } from "../../access-token-provider/errors/access-token-not-found.error";

export default class ClientCookieAccessTokenProvider extends AccessTokenProvider {
  getAccessToken(): AccessToken {
    const jwt = parseCookies()[COOKIE_NAME];
    if (!jwt) {
      throw new AccessTokenNotFoundError();
    }

    return {
      __type: "AccessToken",
      value: jwt,
    };
  }

  setAccessToken(accessToken: AccessToken) {
    setCookie(null, COOKIE_NAME, accessToken.value);
  }
}
