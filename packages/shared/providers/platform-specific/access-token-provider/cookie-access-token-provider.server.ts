import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import { AccessToken } from "../../../models/access-token/access-token";
import AccessTokenProvider, {
  COOKIE_NAME,
} from "../../access-token-provider/access-token-provider.abstract";
import { AccessTokenNotFoundError } from "../../access-token-provider/errors/access-token-not-found.error";

export default class CookieServerAccessTokenProvider extends AccessTokenProvider {
  public getAccessToken(): AccessToken {
    const nextCookies = cookies();
    const token = nextCookies.get(COOKIE_NAME);
    if (!token) {
      throw new AccessTokenNotFoundError();
    }

    return {
      __type: "AccessToken",
      value: token.value,
    };
  }

  public setAccessToken(accessToken: AccessToken) {
    NextResponse.next().cookies.set(COOKIE_NAME, accessToken.value);
  }
}
