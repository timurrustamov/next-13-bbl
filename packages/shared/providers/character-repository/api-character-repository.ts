import { Character } from "../../models/character/character";
import AccessTokenProvider from "../access-token-provider/access-token-provider.abstract";
import CharacterRepository from "./character-repository";

interface CharacterResponse {
  results: Character[];
}

export default class ApiCharacterRepository extends CharacterRepository {
  constructor(private readonly accessTokenProvider: AccessTokenProvider) {
    super();
  }

  async getAll(): Promise<Character[]> {
    this.accessTokenProvider.getAccessToken();
    // token is not really used here eheh
    const result = await fetch("https://rickandmortyapi.com/api/character");
    const json: CharacterResponse = await result.json();

    return json.results;
  }

  async getById(id: string): Promise<Character | null> {
    await new Promise((resolve) => setTimeout(resolve, 5000));
    this.accessTokenProvider.getAccessToken();
    // token is not really used here eheh
    const result = await fetch(
      `https://rickandmortyapi.com/api/character/${id}`
    );
    if (!result.ok) {
      return null;
    }

    return result.json();
  }
}
