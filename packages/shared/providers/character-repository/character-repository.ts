import { Character } from "../../models/character/character";

export default abstract class CharacterRepository {
  abstract getAll(): Promise<Character[]>;
  abstract getById(id: string): Promise<Character | null>;
}
