import { AccessToken } from "../../models/access-token/access-token";

export default class InMemAccessTokenProvider {
  private accessToken: AccessToken | null = null;

  public getAccessToken(): AccessToken | null {
    return this.accessToken;
  }

  public setAccessToken(accessToken: AccessToken | null): void {
    this.accessToken = accessToken;
  }
}
