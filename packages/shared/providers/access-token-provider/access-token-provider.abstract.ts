import { AccessToken } from "../../models/access-token/access-token";

export const COOKIE_NAME = 'GOJOB_JWT_AUTH'; 

export default abstract class AccessTokenProvider {
  abstract getAccessToken(): AccessToken;
  abstract setAccessToken(accessToken: AccessToken): void;
}