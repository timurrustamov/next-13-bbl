export class AccessTokenNotFoundError extends Error {
  static readonly message = "Access token not found";

  constructor() {
    super(AccessTokenNotFoundError.message);
  }
}
